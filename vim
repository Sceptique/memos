Position the cursor where you want to begin cutting.
Press v (or upper case V if you want to cut whole lines).
Move the cursor to the end of what you want to cut.
Press d to cut or y to copy.
Move to where you would like to paste.
Press P to paste before the cursor, or p to paste after. 

The :substitute command searches for a text pattern, and replaces it with a text string.
There are many options, but these are what you probably want:
:%s/foo/bar/g
    Find each occurrence of 'foo' (in all lines), and replace it with 'bar'. 
:s/foo/bar/g
    Find each occurrence of 'foo' (in the current line only), and replace it with 'bar'. 
:%s/foo/bar/gc
    Change each 'foo' to 'bar', but ask for confirmation first. 
:%s/\<foo\>/bar/gc
    Change only whole words exactly matching 'foo' to 'bar'; ask for confirmation. 
:%s/foo/bar/gci
    Change each 'foo' (case insensitive) to 'bar'; ask for confirmation. 
    This may be wanted after using :set noignorecase to make searches case sensitive (the default). 
:%s/foo/bar/gcI
    Change each 'foo' (case sensitive) to 'bar'; ask for confirmation. 
    This may be wanted after using :set ignorecase to make searches case insensitive. 
